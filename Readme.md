# Wallet.Api.Net

C# Wrapper for the [BudgetBakers Wallet API](https://budgetbakersv30apiv1.docs.apiary.io/#) V3.0. 

[![Build Status](https://travis-ci.org/Snowfire01/Wallet.Api.Net.svg?branch=master)](https://travis-ci.org/Snowfire01/Wallet.Api.Net)

#### From Google Play

Wallet is your finance planner that helps you flexibly plan your budget and track spending, so you stay in control and achieve your goals. Actively plan, manage and get a report concerning your finances, together with the people you trust, across multiple currencies, banks and financial institutions. Upload your Loyalty or Reward cards too. Get and keep full control of your finances. Tracking your money just got easier. 

## Getting Started

### Obtain API Token

To use the API, you need an account in the [BudgetBakers Wallet App](https://play.google.com/store/apps/details?id=com.droid4you.application.wallet). There you can access your personal API token under 
```
Settings -> Advanced Settings -> REST API
```

With your E-Mail address and API token as authentication, you can access all functions of the API and manage your account via REST.

Detailed documentation regarding the functionality of the API itself can be found on BudgetBakers' [Apiary documentation page](https://budgetbakersv30apiv1.docs.apiary.io/#).

### Install NuGet Package

Wallet.Api.Net is available as a NuGet package on NuGet.org. 

##### Package Manager Console

````
Install-Package Wallet.Api.Net
````

## Built With

* [Json.NET](https://www.newtonsoft.com/json) - Popular library for Json Serialization in .Net
* [.NET Core](https://dotnet.github.io/) - Platform independent, open source .Net Framework by Microsoft
